const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Item = require("./models/Item");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once("open", async () => {
    try  {
        await db.dropCollection('users');
        await db.dropCollection('items');
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }
    const [user1, user2] = await User.create({
        username: "user1",
        displayName: "Kenny",
        password: "1234",
        phoneNumber: 777212121,
        token: nanoid(),
    }, {
        username: "user2",
        displayName: "Levy",
        password: "4321",
        phoneNumber: 555121212,
        token: nanoid(),
    });
    await Item.create({
        title: "Mario",
        description: "Interesting video game",
        category: "Other",
        price: 5,
        image: "EgoJdx0XGOC9p-X2cDeyM.jpg",
        author: {
            phoneNumber: user1.phoneNumber,
            username: user1.username,
            name: user1.displayName,
        },
    }, {
        title: "Rolls Royce",
        description: "Year 2015, in good condition.",
        category: "Cars",
        price: 2000000,
        image: "qsX__DWD1hthy0m-_d1hh.jpg",
        author: {
            phoneNumber: user2.phoneNumber,
            username: user2.username,
            name: user2.displayName,
        },
    }, {
        title: "Nvidia geforce gtx 1050 ti",
        description: "Graphics card",
        category: "Computers",
        price: 140,
        image: "hy1uE-LGKkr4y6gR8zqXq.jpg",
        author: {
            phoneNumber: user1.phoneNumber,
            username: user1.username,
            name: user1.displayName,
        },
    },{
        title: "17",
        description: "XXXTENTACION's music album (2017)",
        category: "Other",
        price: 5,
        image: "knuPqxxrTeoWYwXSmnjBJ.jpg",
        author: {
            phoneNumber: user2.phoneNumber,
            username: user2.username,
            name: user2.displayName,
        },
    },{
        title: "BMW M5",
        description: "4.4 AT xDrive Competition",
        category: "Cars",
        price: 94500,
        image: "5N8k0MTNU2Fhzghjn0wMJ.jpg",
        author: {
            phoneNumber: user1.phoneNumber,
            username: user1.username,
            name: user1.displayName,
        },
    },{
        title: "Deathadder Essential (RZ01-02540100-R3M1)",
        description: "Computer mouse",
        category: "Computers",
        price: 36,
        image: "N0A_RDZvNEnLpnI_IaZR-.jpg",
        author: {
            phoneNumber: user2.phoneNumber,
            username: user2.username,
            name: user2.displayName,
        },
    })
    db.close();
});