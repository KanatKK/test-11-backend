const router = require("express").Router();
const Item = require("../models/Item");
const multer = require("multer");
const path = require("path");
const config = require("../config");
const {nanoid} = require("nanoid");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

router.get("/", async (req, res) => {
    let query;
    if (req.query.category) {
        query = {category: req.query.category}
    }
    try {
        const items = await Item.find(query);
        res.send(items);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    const result = await Item.findById(req.params.id);
    if (result) {
        res.send(result);
    } else {
        res.status(400).send("Item doesn't find");
    }
});

router.post("/", upload.single("image"), auth, async (req, res) => {
    const itemData = req.body;
    if (req.file) {
        itemData.image = req.file.filename;
    }

    const item = new Item(itemData);
    try {
        item.generateAuthor({
            phoneNumber: req.author.phoneNumber,
            username: req.author.username,
            name: req.author.displayName,
        });
        await item.save();
        res.send(item);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id", auth, async (req, res) => {
    const item = await Item.findById(req.params.id);
    console.log(req.params.id)
    if (req.author.username !== item.author.username) {
        return res.status(403).send("Can't delete");
    }
    try {
        const allItems = await Item.find();
        await Item.findByIdAndDelete(req.params.id);
        res.send(allItems);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;